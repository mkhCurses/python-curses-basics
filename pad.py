#!/usr/bin/env python3

import curses

## pad
screen = curses.initscr()

pad = curses.newpad(50, 50)
pad.addstr("This text is thirty characters")

pad.refresh(0, 2, 0, 0, 15, 20)

curses.napms(5000)
curses.endwin()
