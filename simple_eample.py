#!/usr/bin/env python3

import curses

## simple examples
print("Preparing to initialize screen...")
screen = curses.initscr()
print("Screen initialized")
screen.refresh()

curses.napms(2000)
curses.endwin()

print("Window ended")
