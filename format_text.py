#!/usr/bin/env python3

import curses

## format text
screen = curses.initscr()

curses.start_color()

screen.addstr("Regular text\n")
screen.addstr("Bold\n", curses.A_BOLD)
screen.addstr("Highlighted\n", curses.A_STANDOUT)
screen.addstr("Underline\n", curses.A_UNDERLINE)
screen.addstr("Regular text again\n")

curses.init_pair(1, curses.COLOR_RED, curses.COLOR_WHITE)
screen.addstr("RED ALERT!\n", curses.color_pair(1))

screen.addstr("SUPER RED ALERT!\n", curses.color_pair(1) | curses.A_BOLD | curses.A_UNDERLINE | curses.A_BLINK)

screen.refresh()
curses.napms(8000)

curses.endwin()
