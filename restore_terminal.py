#!/usr/bin/env python3

import curses

## restore terminal
screen = curses.initscr()

# cbreak mode is when no need to press enter for action
curses.cbreak()
# disable cursor
curses.curs_set(0)
# to handle all keyboard keys
screen.keypad(True)

screen.addstr("Hello")
screen.refresh()

curses.napms(2000)

curses.nocbreak()
screen.keypad(False)
curses.echo()

curses.napms(2000)

curses.endwin()
