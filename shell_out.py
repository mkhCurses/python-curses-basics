#!/usr/bin/env python3

import curses
import os
import subprocess

## shell out
screen = curses.initscr()
screen.addstr("Hello! Dropping you in to a command promt...\n")
print("Program initialized...")
screen.refresh()
curses.napms(2000)

curses.endwin()

screen.addstr("I'll be waiting when you get back\n")

print("About to open command promt...")

if os.name == 'nt':
    shell = 'cmd.exe'
else:
    shell = 'sh'
subprocess.call(shell)

screen.refresh()
curses.napms(2000)

curses.endwin()
